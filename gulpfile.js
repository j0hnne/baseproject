var elixir = require('laravel-elixir');
var gulp = require('gulp');
var less = require('gulp-less');
var sourcemaps = require('gulp-sourcemaps');
var autoprefixer = require('gulp-autoprefixer');
var concat = require('gulp-concat');
var notify = require('gulp-notify');
var _ = require('lodash');
var source = require('vinyl-source-stream');
var buffer = require('vinyl-buffer');
var gutil = require('gulp-util');
var uglify = require('gulp-uglify');
var path = require('path');
var plumber = require('gulp-plumber');
var notifier = require('node-notifier');
var jspm = require('jspm');
var browserSync = require('browser-sync').create();
var reload = browserSync.reload;
var sass = require("gulp-sass");
var mocha = require('gulp-mocha');
require('babel/register'); // To preprosses js files for mocha tests


jspm.setPackagePath('.');


var input = {
    less: 'src/less/**/*.less',
    js: 'src/js/main',
    sass: 'src/scss/**/*.scss'
};

var watch = {
    less: input.less,
    js: 'src/js/**/*.js'
};

var output = {
    less: {dir: 'public/css', file: "all.css"},
    sass: {dir: 'public/css', file: "sass.css"},
    js: 'public/js/all.js'
};

gulp.task('js', bundle_js);
gulp.task('less', bundle_less);
gulp.task('serve', serve);
gulp.task('sass', bundle_sass);

gulp.task("test", function() {
    return gulp.src('test/**/*Spec.js')
        .pipe(mocha({ reporter: "spec" })
            .on("error", getErrorHandler("Mocha")))
        .pipe(notify({
            onLast: true,
            title: "Mocha",
            message: "All test succeeded!",
            icon: path.join(__dirname, 'node_modules', 'laravel-elixir', 'icons', 'pass.png')
        }))
});

elixir(function (mix) {
    mix.task('less', watch.less);
    mix.task('js', watch.js);
    mix.task('test', [watch.js, "test/**/*.js"]);
});

gulp.task('tdd', function(){
    gulp.watch([watch.js, "test/**/*.js"], ["test", "js"]);
});


function serve() {
    browserSync.init({
        server: {
            baseDir: "./"
        }
    });
    gulp.watch(output.js, reload);
    gulp.watch(output.less.dir + "/" + output.less.file, reload);
    gulp.watch("*.html").on('change', reload);
    gulp.watch(watch.less, ["less"]);
    gulp.watch(watch.js, ["test", "js"]);
}


function bundle_sass() {

    return gulp.src(input.sass)
        .pipe(sass({sourcemap: true}))
        .pipe(sourcemaps.init())
        .pipe(autoprefixer('last 10 versions'))
        .pipe(concat(output.sass.file))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest(output.sass.dir))
        .pipe(notify({
            onLast: true,
            title: "Sass",
            message: "Created <%= file.relative %>"
        }));
}

function bundle_less() {
    return gulp.src(input.less)
        .pipe(plumber(getErrorHandler("Less")))
        .pipe(sourcemaps.init())
        .pipe(less())
        .pipe(autoprefixer('last 10 versions'))
        .pipe(concat(output.less.file))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest(output.less.dir))
        .pipe(notify({
            onLast: true,
            title: "Less",
            message: "Created <%= file.relative %>"
        }));
}

function bundle_js() {
    return jspm.bundleSFX(input.js, output.js, {mangle: false})
        .then(function () {
            log("jspm.io", "Created all.js");
        }, function (msg) {
            log("jspm.io", getShortMessage(msg), true);
        });
}

function log(title, message, isError) {

    gutil.log(gutil.colors.cyan(title) + ':',
        gutil.colors[isError ? "red" : "green"].call(gutil.colors, message)
    );
    notifier.notify({
        'title': title,
        'message': message,
        'icon': path.join(__dirname, 'node_modules', 'laravel-elixir', 'icons', isError ? 'fail.png' : 'pass.png')
    });
}

function getErrorHandler(title, emitEnd) {
    return function (err) {
        err.title = title;
        err.msg = getShortMessage(err.message);
        notify.onError({
            title: "<%= error.title %>",
            message: "<%= error.msg %>"
        }).call(emitEnd ? this : {}, err); //notify.onError calls this.emit('end')

        this.emit('end');
    }
}

function getShortMessage(message) {
    // Replace absolute path to basename
    return _(('' + message).split(' ')).map(path.basename).join(' ')
}
