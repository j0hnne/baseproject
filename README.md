# BaseProject #

# Installation

## Node
You must first ensure that Node.js is installed on your machine.

`node -v`

## Gulp

Next, you'll want to pull in Gulp as a global NPM package like so:

`npm install --global gulp`

## Dependencies
Initiate the project by install all dependencies with

`npm install`

and

`jspm install`

## Run gulp
In the provided `gulpfile.js`, there is some basic settings done already. To just compile css and js
files, just implement source files in `src/css` and `src/js` and run

`gulp`.

To use the gulp watcher, which is a running process that checks for updated files and update the if it detects changes, just type

`gulp watch`


Run the serve command to serve the webpage with a server that updates on changes in your code.

`gulp serve`

